#include <MsTimer2.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <EEPROM.h>

#define ADDR_PLAYER_NUM    0
#define ADDR_PLAYER_TIME   1

#define LED_R  9
#define LED_G  10
#define LED_B  11

#define INT_SW   2
#define PUSH_SW  3
#define SW_INTERRUPT  0
#define BUTTON_INTERRUPT  1
#define BUTTON_PWR  4

int ledPin[12] = {19,18,17,16,15,14,13,12,8,7,6,5};

#define OFF    0x00
#define RED    0x01
#define BLUE   0x02
#define GREEN  0x04
#define MAGENTA  RED|BLUE
#define YELLOW   RED|GREEN
#define CYAN     BLUE|GREEN
#define WHITE    RED|BLUE|GREEN

int color[6] = {RED, BLUE, GREEN, YELLOW, MAGENTA, CYAN};

#define MAX_PLAYER_NUM 4

// グローバル変数
int _color;
int _ontime, _offtime;
int _timer, _player;
int _onc, _offc;
int _secCnt, _cnt2;
int _playerNum;
int _playerTm[MAX_PLAYER_NUM];

boolean _blink, _blinkflg;
boolean _gotoSleepFlag;
boolean _secFlag;
boolean _powerSwFlag;
boolean _pushSwFlag;
boolean _waitFlag;


// カウントダウン用のLEDを全て消去
void allLedOff()
{
  for(int i=0;i<12;i++){
    digitalWrite(ledPin[i], LOW);
  }
}

// ボタンのLEDを消去
void setButtonLedOff()
{
  pinMode(LED_R, INPUT);
  pinMode(LED_G, INPUT);
  pinMode(LED_B, INPUT);
}

// ボタンの色を設定
void setButtonColor(int color)
{
  _color = color;
  setButtonLedOff();

  boolean oldblink = _blink;

  if(_color&RED){
    pinMode(LED_R, OUTPUT);
    digitalWrite(LED_R, LOW);
  }
  if(_color&BLUE){
    pinMode(LED_B, OUTPUT);
    digitalWrite(LED_B, LOW);
  }
  if(_color&GREEN){
    pinMode(LED_G, OUTPUT);
    digitalWrite(LED_G, LOW);
  }

  _blink = oldblink;
}

// ボタンの色を再設定（ブリンク用）
void setButtonColorAgain()
{
  setButtonColor(_color);
}

// ボタンの点滅のON/OFF
void setButtonBlink(boolean onoff)
{
  _blink = onoff;
  _blinkflg = true;
  _onc = _offc = 0;
}

// ボタンの点滅のパターン設定
void setButtonBlinkPattern(int on, int off)
{
  _ontime = on;
  _offtime = off;
  _onc = _offc = 0;
}


// タイマのクリア
void timerClear()
{
  _secCnt = _cnt2 = 0;
  _onc = _offc = 0;
  _secFlag = false;
}

// タイマ割り込み
void timerInter()
{
  _cnt2++;
  if(_cnt2==10){
    _cnt2 = 0;
    _secCnt++;
    _secFlag = true;
  }
  if(_blink){
    if(_blinkflg){
      // ON中
      _onc++;
      if(_onc == _ontime){
        _onc = 0;
        setButtonLedOff();
        _blinkflg = false;
      }
    }else{
      // OFF中
      _offc++;
      if(_offc == _offtime){
        _offc = 0;
        setButtonColorAgain();
        _blinkflg = true;
      }
    }
  }
}

// タイマの開始
void timerStart()
{
  timerClear();
  MsTimer2::set(100, timerInter);
  MsTimer2::start();
}

// 電源スイッチ割り込み
void powerSwOn()
{
  _powerSwFlag = true;
}

// プッシュボタン割り込み
void buttonOn()
{
  _pushSwFlag = true;
}

// 電源OFF判定（電源スイッチの長押しチェック）
int waitPowerDown()
{
  Serial.println("Waiting to be power down mode");
  unsigned long powerSwTime = millis();
  Serial.println(powerSwTime);
  for(;;){
    if(digitalRead(INT_SW)==HIGH){
      Serial.println("Release power switch before power down");
      _powerSwFlag = false;
      return -1;
    }
    unsigned long nowTime=millis();
    if((nowTime-powerSwTime) > 3000){
      Serial.println(nowTime);
      Serial.println("Go to power down mode");
      _powerSwFlag = false;
      return 0;
    }
  }
}

// スリープモード→レジューム
void sleepAndWakeup()
{
  // all led off
  for(int i=0;i<12;i++){
    digitalWrite(ledPin[i], LOW);
  }
  // ボタンの点滅の停止
  setButtonBlink(false);
  // ボタンの色を順番に変える
  for(int i=0;i<2;i++){
    for(int j=1;j<6;j++){
      setButtonColor(color[j]);
//      toneMelody(mldy_sleep[j]);
      delay(150);
    }
  }
  // ボタンのLED消去
  setButtonLedOff();
  // プッシュボタンではレジュームしない
  detachInterrupt(BUTTON_INTERRUPT);
  // プッシュボタンのプルアップを停止
  digitalWrite(PUSH_SW, LOW);
  pinMode(PUSH_SW, OUTPUT);
  // プッシュボタンの電源オフ
  digitalWrite(BUTTON_PWR, LOW);
  // MsTimer2の停止
  MsTimer2::stop();

  Serial.println("SLEEP");
  delay(500);
  // スリープモードへ
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
//  set_sleep_mode(SLEEP_MODE_IDLE);
  sleep_mode();

  //// ここでスリープ中 ////

  // レジューム
  Serial.println("WAKE UP");
  // プッシュボタンの電源オン
  digitalWrite(BUTTON_PWR, HIGH);
  delay(50);
  // プッシュボタン割り込みの登録
  pinMode(PUSH_SW, INPUT_PULLUP);
  attachInterrupt(BUTTON_INTERRUPT, buttonOn, RISING);
  // 電源ON表示（LEDを回す）
  for(int i=0;i<12;i++){
    digitalWrite(ledPin[i], HIGH);
//    toneMelody(mldy_resume[i]);
    delay(50);
  }
  for(int i=0;i<12;i++){
    digitalWrite(ledPin[i], LOW);
//    toneMelody(mldy_resume[i]);
    delay(50);
  }
  // MsTimer2の起動
  timerStart();
  _powerSwFlag = false;
  _pushSwFlag = false;
}

// ゲーム開始待ち
void waitStart()
{
  Serial.println("Waiting to start");
  // ボタン点滅して開始待ち
  setButtonColor(color[_player]);
  setButtonBlink(false);
  for(;;){
    delay(10);
    if(_powerSwFlag){
      // 電源スイッチが押された
      if(waitPowerDown()==0){
        // 電源ボタン長押しは電源オフ
        _gotoSleepFlag = true;
        return;
      }else{
        // 電源ボタンチョイ押しは次のプレーヤへ
        nextPlayer();
        setButtonColor(color[_player]);
        allLedOff();
      }
    }
    if(_pushSwFlag){
      // ボタンが押されたのでゲーム開始
      while(digitalRead(PUSH_SW)==HIGH){
        if(_powerSwFlag){
          // ボタンが押されている時に電源ボタンも押された
          allLedOff();
          while(digitalRead(INT_SW)==LOW) delay(10);
          _powerSwFlag = false;
          // 制限時間の設定へ！
          setPlayerTime(_player);
          return;
        }
        delay(10);
      }
      // ボタンが押されて離された
      _pushSwFlag = false;
      timerStart();
      Serial.println("Pushed button!");
      return;
    }
  }
}

// ゲームメンバー数の設定
void userSetUp()
{
  Serial.println("Set Number of Members");
  Serial.println(_playerNum);
  delay(500);
  for(;;){
    // 人数のLED表示
    for(int i=0;i<12;i++){
      if(i<_playerNum){
        digitalWrite(ledPin[i], HIGH);
      }else{
        digitalWrite(ledPin[i], LOW);
      }
    }
    if(_powerSwFlag){
      // 電源スイッチが押されたらメンバー数設定から抜ける
      allLedOff();
      while(digitalRead(INT_SW)==LOW) delay(10);
      _powerSwFlag = false;
      return;
    }
    if(_pushSwFlag){
      // ボタンが押されたらメンバー数のインクリメント
      _playerNum++;
      if(_playerNum>MAX_PLAYER_NUM){
        // メンバー数は2名～MAX_PLAYER_NUM
        _playerNum = 2;
      }
      Serial.println(_playerNum);
      EEPROM.write(ADDR_PLAYER_NUM, _playerNum);
      while(digitalRead(PUSH_SW)==HIGH) delay(10);
      _pushSwFlag = false;
    }
    delay(50);
  }
}
void dispPlayerTime(int tm)
{
  allLedOff();
  int n=tm/10;
  for(int i=0;i<n;i++){
    digitalWrite(ledPin[i], HIGH);
  }
}

// プレーヤー毎の制限時間の設定
void setPlayerTime(int i)
{
  Serial.print("Set Timer for Player ");
  Serial.println(i);
  setButtonColor(color[i]);
  setButtonBlink(false);
  dispPlayerTime(_playerTm[i]);
  for(;;){
    if(_powerSwFlag){
      _playerTm[i] += 10;
      if(_playerTm[i]>120){
        _playerTm[i]=10;
      }
      Serial.print("timer =");
      Serial.println(_playerTm[i]);
      EEPROM.write(ADDR_PLAYER_TIME+i, _playerTm[i]);
      dispPlayerTime(_playerTm[i]);

      while(digitalRead(INT_SW)==LOW) delay(10);
      _powerSwFlag = false;
    }
    if(digitalRead(PUSH_SW)==LOW){
      _pushSwFlag = false;
      return;
    }
  }
}



// ゲームの初期設定
void gameInit()
{
  setPlayer(0);
}

// タイムオーバー表示
// return 0=ボタン、1=電源スイッチ
int timeOver()
{
  delay(250);
  // ボタンのLEDを点滅させる
  setButtonColor(color[_player]);
  setButtonBlinkPattern(1,1);
  setButtonBlink(true);
  for(;;){
    for(int i=0;i<4;i++){
      // LEDをぐるぐる回す
      allLedOff();
      digitalWrite(ledPin[i],HIGH);
      digitalWrite(ledPin[i+3],HIGH);
      digitalWrite(ledPin[i+6],HIGH);
      digitalWrite(ledPin[i+9],HIGH);
      delay(100);
      if(_pushSwFlag){
        // ボタンが押されたら抜ける
        allLedOff();
        while(digitalRead(PUSH_SW)==HIGH) delay(10);
        _pushSwFlag = false;
        return 0;
      }
      if(_powerSwFlag){
        // 電源スイッチが押されても抜ける
        allLedOff();
        while(digitalRead(INT_SW)==LOW) delay(10);
        _powerSwFlag = false;
        return 1;
      }
    }
  }
}

// Arduino初期設定ルーチン
void setup()
{
  Serial.begin(9600);
  Serial.println("POWER ON RESET");

  _powerSwFlag = false;
  _pushSwFlag = false;

  // パワーONリセット時は最初にスリープモードに入る
  _gotoSleepFlag = true;

  for(int i=0;i<12;i++){
    digitalWrite(ledPin[i], LOW);
    pinMode(ledPin[i], OUTPUT);
  }

  // ボタンの電源
  digitalWrite(BUTTON_PWR, HIGH);
  pinMode(BUTTON_PWR, OUTPUT);
  delay(50);

  pinMode(INT_SW, INPUT_PULLUP);
  pinMode(PUSH_SW, INPUT_PULLUP);
  timerStart();
  attachInterrupt(SW_INTERRUPT, powerSwOn, FALLING);
  attachInterrupt(BUTTON_INTERRUPT, buttonOn, RISING);

//  // 起動音
//  for(int i=0;i<8;i++){
//    int noteDuration = 1000/mldy_powerOn[i].duration;
//    tone(4, mldy_powerOn[i].note,noteDuration);
//    int pauseBetweenNotes = noteDuration * 1.30;
//    delay(pauseBetweenNotes);
//    noTone(4);
//  }

  // 電池電圧
  Serial.println(analogRead(7));

  // EEPROMから設定の読み込み
  _playerNum = EEPROM.read(ADDR_PLAYER_NUM);
  if((_playerNum<2)||(_playerNum>MAX_PLAYER_NUM)){
    _playerNum = 4;
    EEPROM.write(ADDR_PLAYER_NUM, _playerNum);
  }
  for(int i=0;i<MAX_PLAYER_NUM;i++){
    _playerTm[i] = EEPROM.read(ADDR_PLAYER_TIME+i);
    if((_playerTm[i]<10)||(_playerTm[i]>120)){
      _playerTm[i]=60;
      EEPROM.write(ADDR_PLAYER_TIME+i, _playerTm[i]);
    }
  }
}

// メインループ
void loop()
{
  // 電源スイッチが押されていたら一定時間押され続けるかチェック
  if(_powerSwFlag){
    if(waitPowerDown()==0){
      // 電源ボタン長押しは電源オフ
      _gotoSleepFlag = true;
    }else{
      // 電源ボタンチョイ押しはタイマー停止（ゲーム終了）
      _waitFlag = true;
      allLedOff();
    }
  }

  // 電源OFFフラグが立っていたらスリープ
  if(_gotoSleepFlag){
    _gotoSleepFlag = false;
    sleepAndWakeup();
    if(digitalRead(PUSH_SW)==HIGH){
      // 電源ON時にボタンが押されていたらユーザー数の設定
      while(digitalRead(PUSH_SW)==HIGH) delay(10);
      _pushSwFlag = false;
      userSetUp();
    }
    // ゲーム開始待ち
    gameInit();
    _waitFlag = true;
  }

  if(_waitFlag){
    waitStart();
    _waitFlag = false;
    startTurn();
  }

  if(_secFlag){
    // 1秒毎処理
    _secFlag = false;

    // 残り時間デクリメント
    _timer--;
    Serial.println(_timer);

    if(_timer==30){
      // 残り30秒
//      tone(BUZZER, NOTE_C5, 500);
    }

    if((_timer%5)==0){
      setTimeLed(_timer/5);
    }

//    if(_timer<=12){
//      // 残り時間12秒以下で残時間表示
//      setTimeLed(_timer);
//    }
    if(_timer==0){
      // 残り時間ゼロでタイムオーバー表示
      setTimeLed(0);
      if(timeOver()==0){
        // ボタン
        nextPlayer();
        startTurn();
      }else{
        // 電源スイッチ
        _waitFlag = true;
      }
    }
    if(_timer==5){
      // 残り5秒から点滅を速くする
      setButtonBlinkPattern(1,1);
    }
  }

  if(_pushSwFlag){
    // ボタンが押されたら次のプレーヤーのターン
    if(digitalRead(PUSH_SW)==LOW){
      _pushSwFlag = false;
      nextPlayer();
      startTurn();
    }
  }
  delay(10);
}

// 残時間表示
void setTimeLed(int time)
{
  allLedOff();
//  int n = 13 - time;
  int n = 12 - time;
  for(int i=0;i<n;i++){
    digitalWrite(ledPin[i], HIGH);
  }
}

// プレーヤの設定
void setPlayer(int player)
{
  _player = player;
  Serial.print("Player:");
  Serial.println(_player);
}

// ターン開始
void startTurn()
{
  Serial.print("Start Player ");
  Serial.print(_player);
  Serial.println("'s turn.");
  timerStart();
  _timer = _playerTm[_player];
  setTimeLed(_timer/5);
  setButtonColor(color[_player]);
  setButtonBlinkPattern(9,1);
  setButtonBlink(true);
}


// 次のプレーヤーへ
void nextPlayer()
{
  _player++;
  if(_player >= _playerNum){
    _player = 0;
  }
  setPlayer(_player);
}

