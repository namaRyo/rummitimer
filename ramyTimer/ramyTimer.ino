#include <MsTimer2.h>


#define LED_R  9
#define LED_G  10
#define LED_B  11

#define INT_SW   2
#define PUSH_SW  3

int ledPin[12] = {5,19,18,17,16,15,14,13,12,8,7,6};

int r,g,b;
int count,color;
boolean flg;

#define OFF    0x00
#define RED    0x01
#define BLUE   0x02
#define GREEN  0x04
#define MAGENTA  RED|BLUE
#define YELLOW   RED|GREEN
#define CYAN     BLUE|GREEN
#define WHITE    RED|BLUE|GREEN

int _color;
boolean _blink, _blinkflg;
int _ontime, _offtime;
void setButtonLedOff()
{
  pinMode(LED_R, INPUT);
  pinMode(LED_G, INPUT);
  pinMode(LED_B, INPUT);
}
void setButtonColor(int color)
{
  _color = color;
  setButtonLedOff();

  if(_color&RED){
    pinMode(LED_R, OUTPUT);
    digitalWrite(LED_R, LOW);
  }
  if(_color&BLUE){
    pinMode(LED_B, OUTPUT);
    digitalWrite(LED_B, LOW);
  }
  if(_color&GREEN){
    pinMode(LED_G, OUTPUT);
    digitalWrite(LED_G, LOW);
  }
}
void setButtonColorAgain()
{
  setButtonColor(_color);
}
void setButtonBlink(boolean onoff)
{
  _blink = onoff;
  _blinkflg = true;
}
void setButtonBlinkPattern(int on, int off)
{
  _ontime = on;
  _offtime = off;
}


int secCount,cnt2;
boolean secFlag;
int _onc, _offc;
void timerClear()
{
  secCount = cnt2 = 0;
  _onc = _offc = 0;
  secFlag = false;
}

void timerInter()
{
  cnt2++;
  if(cnt2==10){
    cnt2 = 0;
    secCount++;
    secFlag = true;
  }
  if(_blink){
    if(_blinkflg){
      // ON中
      _onc++;
      if(_onc == _ontime){
        _onc = 0;
        setButtonLedOff();
        _blinkflg = false;
      }
    }else{
      // OFF中
      _offc++;
      if(_offc == _offtime){
        _offc = 0;
        setButtonColorAgain();
        _blinkflg = true;
      }
    }
  }
}

void timerStart()
{
  timerClear();
  MsTimer2::set(100, timerInter);
  MsTimer2::start();
}

void setup()
{
  count = 0;
  color = 0;
  flg = true;
  
  for(int i=0;i<12;i++){
    digitalWrite(ledPin[i], LOW);
    pinMode(ledPin[i], OUTPUT);
  }
  pinMode(INT_SW, INPUT_PULLUP);
  pinMode(PUSH_SW, INPUT_PULLUP);

  timerStart();

  setButtonColor(RED);
  setButtonBlinkPattern(1,19);
  setButtonBlink(true);

  delay(5000);
}

void loop()
{
  if(secFlag){
    secFlag = false;
    
    if(flg){
      digitalWrite(ledPin[count], LOW);
    }else{
      digitalWrite(ledPin[count], HIGH);
    }
    if(count>=9)
      setButtonBlinkPattern(1,1);

    count++;
    if(count>=12){
      count = 0;
      flg = !flg;
      color++;
      if(color==8) color=0;
      setButtonColor(color);
      setButtonBlinkPattern(1,9);
      setButtonBlink(true);
    }
  }
  delay(10);
}



